import Vue from 'vue'
import Router from 'vue-router'
import home from '@/pages/index'
import softwareSevice from '@/pages/softwareSevice'
import solution from '@/pages/solution'
import outsource from '@/pages/outsource'
import cloud from '@/pages/cloud'
import about from '@/pages/about'
import productDetail from '@/pages/productDetail'

Vue.use(Router)

const router = new Router({
  routes: [
    {path: '/', name: 'home', component: home,meta:{title:'首页'}},
    {path: '/softwareSevice', name: 'softwareSevice', component: softwareSevice,meta:{title:'软件服务'}},
    {path: '/solution', name: 'solution', component: solution,meta:{title:'解决方案'}},
    {path: '/outsource', name: 'outsource', component: outsource,meta:{title:'人力外包'}},
    {path: '/cloud', name: 'cloud', component: cloud,meta:{title:'云服务'}},
    {path: '/about', name: 'about', component: about,meta:{title:'关于我们'}},
    {path: '/productDetail', name: 'productDetail', component: productDetail,meta:{title:'产品详情'}},
  ],
  scrollBehavior(to, from, saveTop) {
    if (saveTop) {
      return saveTop;
    } else {
      return { x: 0, y: 0 }
    }
  },
})

router.beforeEach((to,from,next)=>{
  document.title = to.meta.title;
  next()
})
export default router
