
export default  {
    data(){
        return {
            codeText:'获取验证码',
            budget:[],//预算
            budgetIndex:-1,
            service:[],//服务类型
            serviceIndex:-1,
            budgetShow:false,
            serviceShow:false,
            telError:true,

            budgetText:'',
            serviceText:'',
            form:{
                phone:'',
                yzm:'',
                type_id:'',
                yusuan_id:'',
                content:''
            },
            isSend:true,
            timer:'',
            isSubmit:true,
            isAgree:false
        }
    },
    
    mounted(){
        this.getYuSuan();
        this.getService()
    },
    methods:{
        // 预算
        getYuSuan(){
            this.$http.post('index/getyusuan').then(res=>{
              this.budget = res;
            })
          },
        // 获取表单服务类型
        getService(){
            this.$http.post('index/gettype').then(res=>{
              console.log(res)
              this.service = res;
            })
          },
        //项目预算状态改变
        changeBudget(index){
            if(index != -1){
              this.form.yusuan_id = this.budget[index].id;
              this.budgetText = this.budget[index].name;
            }else{
              this.budgetIndex = -1;
              this.form.yusuan_id = '';
              this.budgetText = ''
            }
            
            this.budgetShow = !this.budgetShow;
            this.serviceShow = false;
            console.log(this.budgetShow)
          },
          //服务项目状态改变
          changeService(index){
            if(index != -1){
              this.form.type_id = this.service[index].id;
              this.serviceText = this.service[index].name
            }else{
              this.serviceIndex = -1;
              this.form.type_id = '';
              this.serviceText = ''
            }
            this.serviceShow = !this.serviceShow;
            this.budgetShow = false
          },
        sendCode(){
            console.log(this.form.phone)
            if(!this.isSend) return;
            if(!this.form.phone){
                this.$toast('手机号不能为空');
                return
            }
            if(!(/^1[3456789]\d{9}$/.test(this.form.phone))){
                this.$toast('手机号格式不正确');
                this.telError = true;
                return
            }
            this.telError = false;
            this.isSend = false;
            this.$http.post('index/getyzm',{
                phone:this.form.phone
            }).then(res=>{
                let s = 60;
                this.codeText = s+'s后重发'
                this.timer = setInterval(()=>{
                    s--;
                    this.codeText = s+'s后重发';
                    if(s == 0){
                        this.isSend = true;
                        this.codeText = "重新发送";
                        clearInterval(this.timer)
                    }
                },1000)
            }).catch(err=>{
                this.$toast('发送失败');
                this.isSend = true;
            })
        },
        submitForm(type){
            if(!this.isSubmit) return
            if(!type){
               
                this.form.type_id = 1;
                this.form.yusuan_id = 1;

            }
            if(!this.form.content){
                this.$toast('请输入您的需求内容');
                return
            }
            if(!this.form.yusuan_id) {
                this.$toast('请选择预算');
                return
            }
            if(!this.form.type_id) {
                this.$toast('请选择服务类型');
                return
            }
            if(!this.form.phone){
                this.$toast('请输入您的手机号');
                return
            }
            if(!(/^1[3456789]\d{9}$/.test(this.form.phone))){
                this.$toast('手机号格式不正确');
                this.telError = true;
                return
            }
            if(!this.form.yzm){
                this.$toast('请输入验证码');
                return
            }
            if(!type){
                if(!this.isAgree){
                    this.$toast('请同意相关协议');
                    return
                }
            }
            this.isSubmit = false
            this.$http.post('index/setxuqiu',this.form).then(res=>{
                this.$toast('提交成功');
                this.form = {};
                this.isSubmit = true;
            }).catch(err=>{
                console.log(err)
                this.$toast(err.msg);
                this.isSubmit = true;
            })
        }
    },
    destroyed(){
        clearInterval(this.timer)
    }
}