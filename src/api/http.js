import axios from 'axios'
import md5 from 'js-md5'
import config from './config.js'

class http {
  post(url, params) {
    return new Promise((resolve, rejects) => {
       let data = params ? params : {};
      let timestamp = new Date().getTime().toString();
      let random = Math.random().toString();
      let sign =  md5('WEKJGHDFDF' + random + timestamp);
     
      data.timestamp = timestamp;
      data.random = random;
      data.sign = sign;
      axios({
        method: 'post',
        url:config.url+ '/' +url,
        data: params,
        headers:{
            'Accept': 'application/json'
        },
      }).then(res => {
        if (res.data.code === 1) {
          resolve(res.data.msg)
        } else {
          rejects(res.data)
        }
      }).catch(err => {
        rejects(err)
        console.log('数据调用失败')
      })
    })
  };
  get(url, params) {
    return new Promise((resolve, rejects) => {
      let data = params ? params : {};
      let timestamp = new Date().getTime().toString();
      let random = Math.random().toString();
      let sign =  md5('WEKJGHDFDF' + random + timestamp);
     
      data.timestamp = timestamp;
      data.random = random;
      data.sign = sign;
      axios({
        method: 'get',
        url:config.url+'/'+ url,
        params: params,
        headers: {
          'Accept':'application/json',
        },
        timeout: 15000
      }).then(res => {
        if (res.status === 200) {
          resolve(res.data)
        } else {
          rejects(res.data)
        }
      }).catch(err => {
        rejects(err)
      })
    })
  }
}

export default new http()