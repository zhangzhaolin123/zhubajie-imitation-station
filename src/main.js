// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import 'lib-flexible/flexible'

import { Toast } from 'vant';

import config from './api/config.js'
Vue.prototype.$url = config.imgurl;
import http from './api/http.js';
Vue.prototype.$http = http;
Vue.use(Toast);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
